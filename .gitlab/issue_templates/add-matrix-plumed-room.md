Please plumb the following matrix room(s) to its IRC counterpart(s) on oftc:

 - 

Additional notes, if any:


## Check-list for Debian Social Team

* [ ] Create the Matrix room(s)
* [ ] Link the room to IRC within the matrix-appservice-irc config bundle
* [ ] Deploy the changes
* [ ] Invite social team and @mjolnir:matrix.debian.social as admins
* [ ] Invite requester as admin if applicable
* [ ] Add room to #debian:matrix.debian.social space

----
/label matrix
/cc @jcc @paddatrapper @rhonda @stefanor
