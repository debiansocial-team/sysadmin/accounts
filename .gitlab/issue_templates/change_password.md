Please reset my password for the following service(s):

(Delete all that doesn't apply)

 - pixelfed.debian.social
 - pleroma.debian.social
 - peertube.debian.social

/label account
/cc @jcc @paddatrapper @rhonda
/confidential