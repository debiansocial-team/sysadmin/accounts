Hi, unfortunately we haven't figured out how to deal with -guest usernames yet,
because it's not possible to migrate from a -guest username when you become a DD
on these services. Even if we renamed a user in the database, it would break
subscriptions on federated services.

In the meantime we can offer you the following options:

1. We create your usernames with the -guest suffix, and when you become a DD,
   we just create a new account for you without the suffix. It's likely that we
   won't be able to do any kind of migration.

2. The services we use have many public instances where you can sign up and
   interact with the users on debian.social. This might be a compromise, but at
   least you have this option too.

We'll try to make this better, and sorry for the inconvenience in the meantime.
If you'd like to go forward with option 1, let us know with your email address
and we'll proceed.
